﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayEgg : MonoBehaviour
{
    float spaceBar;
    bool released = true;
    public GameObject egg;
    public float moveSpeed;
    float horizontalDirection = 1;
    float verticalDirection;
    int[] threeDirections = new int[] { -1, 0, 1};
    int[] twoDirections = new int[] { -1, 1 };

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        spaceBar = Input.GetAxis("Jump");
        if (spaceBar != 0 && released == true)
        {
            //This is a note to capture a change to script in Git. Version 2
            
            //TODO: raytrace to check where is available to move to next. Also prevent moving off edge of screen

            //TODO:select from a location to move to - replace temp code below
                //Pick horizonal axis movement -1, 0 or 1

                horizontalDirection = threeDirections[Random.Range(0, 3)];

                //Pick vertical axis movement -1, 0 or 1. Cannot be 0 if horizontal axis movement is already 0
                if (horizontalDirection != 0)
                {
                    verticalDirection = threeDirections[Random.Range(0, 3)];
                }
                else
                {
                    verticalDirection = twoDirections[Random.Range(0, 2)];
                }
            
            //make an egg at your current position
            GameObject newEgg = Instantiate(egg);
            newEgg.transform.position = transform.position;
            newEgg.transform.SetParent(null);
            
            //TODO: play a sound

            //TODO: increment score

            transform.Translate(new Vector3(horizontalDirection, verticalDirection, 0) * moveSpeed);
            released = false;
        }
        else if (spaceBar == 0)
        {
            released = true;
        }
    }
}
